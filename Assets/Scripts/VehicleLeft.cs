﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleLeft : MonoBehaviour {


	public VehicleManager theVehicleManager;
	float speed = .04f;


	void Start () {
		
	}


	void FixedUpdate () {
		
		MoveAcrossScreen();

		}

	void MoveAcrossScreen () {
		
		gameObject.transform.Translate (-speed, 0f, 0f);

		}


	void OnTriggerEnter2D(Collider2D col) {
		if (col.gameObject.tag == "Shredder") {
			Destroy (gameObject);
		}
	}
}
