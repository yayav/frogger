﻿using System.Collections; 
using System.Collections.Generic; 
using UnityEngine;  

public class FrogLog : MonoBehaviour  {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay2D(Collider2D collStay) {

		if (collStay.name == "Frogger")
		print ("STAY-- onRide");
		collStay.transform.parent = transform;
	}

	void OnTriggerExit2D(Collider2D collExit) {
		print ("EXIT -- offRide");
		collExit.transform.parent = null;        
	}
}

