﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurtleLog : MonoBehaviour {

	public GameObject logTurtlePrefab;
	public GameObject waterPrefab;
	public GameObject playerFrogger;
	public float time = 1;
	public float tutlelogVectorX;
	public float waterVectorX;
	public float speed;

	void Start () {
		InvokeRepeating ("MakeNew", 0, time+speed);
	}

	void MakeNew () {
		Instantiate (logTurtlePrefab, new Vector3 (tutlelogVectorX, transform.position.y, transform.position.z), Quaternion.identity);
		Instantiate (waterPrefab, new Vector3 (waterVectorX, transform.position.y, transform.position.z), Quaternion.identity);
	}

	void OnTriggerEnter2D(Collider2D col) {
		if (col.gameObject.tag == "Shredder") {
			Destroy (gameObject);
		}
	}
}
