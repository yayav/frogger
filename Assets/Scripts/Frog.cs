﻿using System.Collections;

using System.Collections.Generic;
using UnityEngine;

public class Frog : MonoBehaviour {

	float rightFrame = 7f;
	float leftFrame = -7f;
	float topFrame = 8f;
	float bottomFrame = -8f;
	float offset = 1f;
	int count = 0;
	bool inWater = false;
	bool isDrown = false;
	bool isOnRide = false; 
	bool isDead = false;
	bool haveAdjustedY = false;
	bool haveMoved = false;
	bool resetParent = true;
	public GameObject player;
	public GameObject background;
	public AudioClip[] audio = new AudioClip[3];

	void Start (){
		AudioSource audio = GetComponent<AudioSource>();
	}


	void Update () {
		//Basic controls
		count = count + 1;

		if (count >= 390) {
			//background.gameObject.GetComponent<Renderer>().material.color = new Color (255,255,255,255);
			background.SetActive (false);

			if (Input.GetKeyDown (KeyCode.UpArrow)&& gameObject.transform.position.y < topFrame-offset) {
				gameObject.transform.Translate (0f, 1f, 0f);
				PlaySound ();

			} else if (Input.GetKeyDown (KeyCode.DownArrow)&& gameObject.transform.position.y > bottomFrame+offset) {
				gameObject.transform.Translate (0f, -1f, 0f);
				PlaySound ();
			}

			if (Input.GetKeyDown (KeyCode.LeftArrow)&& gameObject.transform.position.x > leftFrame+offset) {
				gameObject.transform.Translate (-1f, 0f, 0f);
				PlaySound ();

			} else if (Input.GetKeyDown (KeyCode.RightArrow)&& gameObject.transform.position.x < rightFrame-offset) {
				gameObject.transform.Translate (1f, 0f, 0f);
				PlaySound ();
			}
		}
	}


	void OnCollisionEnter2D (Collision2D colEnter) {
		//Collide with cars
		if (gameObject.transform.position.y <= -1) {
			player.SetActive(false);
			print ("Collide with: CARS");
			GetComponent<AudioSource>().clip = audio[2];
			GetComponent<AudioSource>().Play();
		}

		//Collide with Log or Turtle
		if (colEnter.gameObject.tag == "TurtleOrLog") {
			player.transform.parent = colEnter.gameObject.transform;
			print ("On Collision ENTER ---- 1 " + colEnter.gameObject.name);
		}

		//Collide with water
		if (colEnter.gameObject.tag == "Water") {
			player.SetActive (false);
			print ("WATER");
			GetComponent<AudioSource>().clip = audio[1];
			GetComponent<AudioSource>().Play();
		}

		if (colEnter.gameObject.tag == "Win") {
			colEnter.gameObject.GetComponent<Renderer>().enabled = true;
			player.transform.position = new Vector3 (0f, -6.55f, 0f) ;
		}
	}

	void OnCollisionStay2D (Collision2D colStay) {
		if (colStay.gameObject.tag == "TurtleOrLog") {
			player.transform.parent = colStay.gameObject.transform;
			print ("On Collision STAY ---- 2 " + colStay.gameObject.name);
			//ExitFunction ();
		}

		if (colStay.gameObject.tag == "Water") {
			player.SetActive (false);
			print ("WATER");
			GetComponent<AudioSource>().clip = audio[1];
			GetComponent<AudioSource>().Play();
		}
		if (colStay.gameObject.tag == "Win") {
			colStay.gameObject.GetComponent<Renderer>().enabled = true;
			player.transform.position = new Vector3 (0f, -6.55f, 0f) ;
		}
	}

	void OnCollisionExit2D (Collision2D colExit) {
		if (colExit.gameObject.tag == "TurtleOrLog") {
			player.transform.parent = null;
			print ("On Collision EXIT ---- 3 " + colExit.gameObject.name);
		}
	}

	void ExitFunction (){
		player.transform.parent = null;
	}
		
	void PlaySound (){
		GetComponent<AudioSource>().clip = audio[0];
		GetComponent<AudioSource>().Play();
	}
}
	
