﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogManager : MonoBehaviour {


	public GameObject logPrefab;
	public GameObject player;
	public float time = 1;

	int numberOfLogs;
	TextMesh hitFrogText;


	//List<Vehicle> listOfVehicles;


	void Start () {
		var randomSpace = Random.Range (1, 4);
		InvokeRepeating ("MakeLog", 0, time+randomSpace);
	}

	void MakeLog () {
		Instantiate (logPrefab, transform.position, Quaternion.identity);
	}
}
